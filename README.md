Bibliotecas
'com.google.android.material:material:1.0.0' - Components de Material Design
'com.github.satyan:sugar:1.3' - Sugar ORM, para interação com banco de dados
'com.squareup.retrofit2:retrofit:2.6.0' - Retrofit, para gerenciamento de Requisições para API
'com.squareup.retrofit2:converter-gson:2.6.0' - Conversor do Retrofit
'org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.2' - Biblioteca de Coroutines do Kotlin, para execução de ações assíncronas
'com.github.bumptech.glide:glide:4.10.0' - Glide, para gerenciamento de Imagens

Implementações a serem feitas
- Implementação da ferramenta de busca de Empresas
- Implementação de listagem vazia (Retorno na tela para Usuário)

Uso da aplicação
- Realizar login com email e senha
- Esperar o carregamento da listagem de empresas
- Consultar os detalhes da empresa clicando no item da mesma