package com.enosnery84.empresasandroid

import com.enosnery84.empresasandroid.services.RequestService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInitializer {

      private val retrofit = Retrofit.Builder()
            .baseUrl("https://empresas.ioasys.com.br/api/v1/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    fun requestService() = retrofit.create(RequestService::class.java)

}