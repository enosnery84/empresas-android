package com.enosnery84.empresasandroid.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.enosnery84.empresasandroid.R
import com.enosnery84.empresasandroid.model.Enterprise
import kotlinx.android.synthetic.main.enterprise_list_item.view.*

class EnterpriseListAdapter(private val context : Context, private val data : ArrayList<Enterprise>) : BaseAdapter() {

    private val inflater : LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val enterprise = getItem(p0) as Enterprise
        val row = inflater.inflate(R.layout.enterprise_list_item, p2, false)
        val nameTextView = row.enterprise_name as TextView
        val businessTextView = row.enterprise_business as TextView
        val locationTextView = row.enterprise_location as TextView
        val imageView = row.list_item_image as ImageView

        nameTextView.text = enterprise.enterprise_name
        businessTextView.text = enterprise.enterprise_type.enterprise_type_name
        locationTextView.text = enterprise.country
        Glide.with(context).load(enterprise.photo).into(imageView)

        return row
    }

    override fun getItem(p0: Int): Any {
        return data[p0]
    }

    override fun getItemId(p0: Int): Long {
        return data[p0].id
    }

    override fun getCount(): Int {
        return data.size
    }
}