package com.enosnery84.empresasandroid.model

import com.orm.SugarRecord
import java.io.Serializable

class Enterprise(
    val enterprise_name : String,
    val description : String,
    val email_enterprise : String,
    val facebook : String,
    val twitter: String,
    val linkedin : String,
    val phone : String,
    val own_enterprise : String,
    val photo : String,
    val value : Float,
    val shares : Float,
    val share_price : Float,
    val own_shares : Float,
    val city : String,
    val country : String,
    val enterprise_type : EnterpriseType,
    val success : Boolean
) : SugarRecord<Enterprise>(), Serializable