package com.enosnery84.empresasandroid.model

import com.orm.SugarRecord
import java.io.Serializable

class EnterpriseType(
//    @SerializedName("id")
//    val enterpriseTypeId : Int,
    val enterprise_type_name : String
) : SugarRecord<EnterpriseType>(), Serializable