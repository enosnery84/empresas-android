package com.enosnery84.empresasandroid.model

class Investor(
    val id : Int,
    val investor_name : String,
    val email : String,
    val city : String,
    val country : String,
    val balance : Float,
    val photo : String,
    val portfolio: Portfolio,
    val portfolio_value : Float,
    val first_access : Boolean,
    val super_angel : Boolean
){
    override fun toString(): String {
        return "Investor(id=$id, investor_name='$investor_name', email='$email', city='$city', country='$country', balance=$balance, photo='$photo', portfolio=$portfolio, portfolio_value=$portfolio_value, first_access=$first_access, super_angel=$super_angel)"
    }
}