package com.enosnery84.empresasandroid.model

class Portfolio (
    val enterprisesNumber : Int,
    val enterprises : List<Any>
)