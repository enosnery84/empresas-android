package com.enosnery84.empresasandroid.model

import com.orm.SugarRecord
import java.io.Serializable

class User(
    var email: String? = "",
    var accessToken: String? = "",
    var client: String? = "",
    var uid: String? = ""
) : SugarRecord<User>(), Serializable{
    override fun toString(): String {
        return "User(email='$email', accessToken='$accessToken', client='$client', uid='$uid')"
    }
}