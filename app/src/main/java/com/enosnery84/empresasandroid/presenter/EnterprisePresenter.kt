package com.enosnery84.empresasandroid.presenter

import android.content.Context
import android.content.Intent
import com.enosnery84.empresasandroid.RetrofitInitializer
import com.enosnery84.empresasandroid.model.Enterprise
import com.enosnery84.empresasandroid.model.User
import com.enosnery84.empresasandroid.requests.EnterpriseResponse
import com.enosnery84.empresasandroid.view.DetailActivity

class EnterprisePresenter {

   suspend fun loadEnterprises(user : User): ArrayList<Enterprise> {
        val response = RetrofitInitializer().requestService().listEnterprises(user.accessToken!!,
            user.client!!, user.uid!!)
        val enterpriseResponse = response.body() as EnterpriseResponse
        return enterpriseResponse.enterprises

    }

    fun getDetailIntent(enterprise: Enterprise, context: Context) : Intent?{
        return DetailActivity.newIntent(context, enterprise)
    }
}