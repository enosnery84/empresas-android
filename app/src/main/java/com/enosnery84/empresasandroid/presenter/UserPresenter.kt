package com.enosnery84.empresasandroid.presenter

import android.content.Context
import android.content.Intent
import android.util.Log
import com.enosnery84.empresasandroid.RetrofitInitializer
import com.enosnery84.empresasandroid.model.User
import com.enosnery84.empresasandroid.requests.UserRequest
import com.enosnery84.empresasandroid.view.ListActivity
import com.orm.SugarRecord

class UserPresenter {
    var success = false
   suspend fun performLogin(login : String, password : String, context : Context): Intent? {
        val userRequest = UserRequest(login, password)
        var userId = ""
        val response = RetrofitInitializer().requestService().login(userRequest)
        if (response.isSuccessful) {
            val user = response.body()
            val headers = response.headers()
            val loggedUser = User(user?.investor?.email,headers.get("access-token"), headers.get("client"), headers.get("uid"))
            loggedUser.save()
            userId = loggedUser.id.toString()
            success = true

        } else {
            Log.e("LoginServiceError", response.message())
            success = false
        }
        if(success) {
            val tempUser = SugarRecord.find(User::class.java, "id = ?", userId)[0]
            return ListActivity.newIntent(context, tempUser)
        }else{
            return null
        }


    }
}