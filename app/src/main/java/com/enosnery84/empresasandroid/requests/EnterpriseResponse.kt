package com.enosnery84.empresasandroid.requests

import com.enosnery84.empresasandroid.model.Enterprise

class EnterpriseResponse (
    val enterprises : ArrayList<Enterprise>
)