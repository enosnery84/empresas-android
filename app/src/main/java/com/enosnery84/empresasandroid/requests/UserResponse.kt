package com.enosnery84.empresasandroid.requests

import com.enosnery84.empresasandroid.model.Investor

class UserResponse(
    val investor: Investor,
    val enterprise : String,
    val success : Boolean
){
    override fun toString(): String {
        return "UserResponse(investor=$investor, enterprise='$enterprise', success=$success)"
    }
}