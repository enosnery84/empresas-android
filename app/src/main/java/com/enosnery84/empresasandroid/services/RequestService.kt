package com.enosnery84.empresasandroid.services

import com.enosnery84.empresasandroid.requests.EnterpriseResponse
import com.enosnery84.empresasandroid.requests.UserRequest
import com.enosnery84.empresasandroid.requests.UserResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface RequestService {
        @POST("users/auth/sign_in")
        suspend fun login(@Body userRequest: UserRequest) : Response<UserResponse>

        @GET("enterprises")
        suspend fun listEnterprises(@Header("access-token") accesstoken: String,
                                    @Header("client") client : String,
                                    @Header("uid") uid : String) : Response<EnterpriseResponse>
}