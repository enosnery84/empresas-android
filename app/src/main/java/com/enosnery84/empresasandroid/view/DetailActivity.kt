package com.enosnery84.empresasandroid.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.enosnery84.empresasandroid.R
import com.enosnery84.empresasandroid.model.Enterprise
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {

    private val context = this

    companion object {
        const val EXTRA_ENTERPRISE = "enterprise"

        fun newIntent(context: Context, enterprise: Enterprise): Intent {
            val detailIntent = Intent(context, DetailActivity::class.java)
            detailIntent.putExtra(EXTRA_ENTERPRISE, enterprise)
            return detailIntent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        val enterprise = intent.extras?.getSerializable(EXTRA_ENTERPRISE) as Enterprise
        Glide.with(context).load(enterprise.photo).into(enterprise_image)
        enterprise_name.text = enterprise.enterprise_name
        enterprise_description.text = enterprise.description
        arrow_back.setOnClickListener {
            onBackPressed()
            finish()
        }
    }

}