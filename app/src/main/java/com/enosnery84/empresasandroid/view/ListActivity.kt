package com.enosnery84.empresasandroid.view

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import com.enosnery84.empresasandroid.R
import com.enosnery84.empresasandroid.adapter.EnterpriseListAdapter
import com.enosnery84.empresasandroid.model.Enterprise
import com.enosnery84.empresasandroid.model.User
import com.enosnery84.empresasandroid.presenter.EnterprisePresenter
import kotlinx.android.synthetic.main.activity_list.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ListActivity : AppCompatActivity() {


    companion object {
        const val EXTRA_USER = "user"


        fun newIntent(context: Context, user: User): Intent {
            val listIntent = Intent(context, ListActivity::class.java)
            listIntent.putExtra(EXTRA_USER, user)
            return listIntent
        }
    }


    private val context = this
    private val enterprisePresenter = EnterprisePresenter()
    lateinit var enterprises : MutableList<Enterprise>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        pBar.visibility = View.GONE
        actionBar?.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.colorAccent, theme)))
        val loggedUser = intent.extras?.getSerializable(EXTRA_USER) as User
            pBar.visibility = View.VISIBLE
            GlobalScope.launch {
                enterprises = enterprisePresenter.loadEnterprises(loggedUser)
                context.runOnUiThread {
                    val adapter =
                        EnterpriseListAdapter(context, enterprises as ArrayList<Enterprise>)
                    pBar.visibility = View.GONE
                    list_view.adapter = adapter
                }
        }

        list_view.setOnItemClickListener { _, _, position, _ ->
            val intent = enterprisePresenter.getDetailIntent(enterprises[position],context)
            Log.e("Teste", intent.toString())
            Log.e("Teste", enterprises[position].enterprise_name)
            if(intent != null){
                startActivity(intent)
            }
        }

    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.options_menu, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu?.findItem(R.id.search)?.actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
        }
        return true
    }

}