package com.enosnery84.empresasandroid.view

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.enosnery84.empresasandroid.presenter.UserPresenter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch



class MainActivity : AppCompatActivity() {

    private val context = this
    private val userPresenter = UserPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.enosnery84.empresasandroid.R.layout.activity_main)
        pBar.visibility = View.GONE

        login_button.setOnClickListener {
            pBar.visibility = View.VISIBLE
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(login_button.windowToken, 0)
            GlobalScope.launch {
                val intent = userPresenter.performLogin(
                    login_edit.text.toString(),
                    password_edit.text.toString(),
                    context
                )
                if (intent != null) {
                    startActivity(intent)
                } else {
                    context.runOnUiThread(Runnable {
                        pBar.visibility = View.GONE
                        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(login_button.windowToken, 0)
                        Toast.makeText(context, "Login Inválido", Toast.LENGTH_SHORT).show()
                    })

                }
            }
        }
    }
}
